import { userController } from "../Controller/user";

export class Routes {
  private userController = new userController();

  public initialRoutes(app: any): void {
    console.log("routes");
    app.route("/user").post(this.userController.CreateUser);
    app.route("/user").get(this.userController.getuser);

    app.route("/user/:id").put(this.userController.Update);
    app.route("/user/:id").delete(this.userController.delete);
  }
}
