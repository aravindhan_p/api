"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
var user_1 = require("../Controller/user");
var Routes = /** @class */ (function () {
    function Routes() {
        this.userController = new user_1.userController();
    }
    Routes.prototype.initialRoutes = function (app) {
        console.log("routes");
        app.route("/user").post(this.userController.CreateUser);
        app.route("/user").get(this.userController.getuser);
        app.route("/user/:id").put(this.userController.Update);
        app.route("/user/:id").delete(this.userController.delete);
    };
    return Routes;
}());
exports.Routes = Routes;
