"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var userschema = new mongoose_1.default.Schema({
    name: { type: String },
    leavereason: { type: Number },
    fromdate: { type: Number },
    todate: { type: Number },
});
var Mongodb = mongoose_1.default.model("user", userschema);
exports.default = Mongodb;
