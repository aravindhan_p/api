import mongoose from "mongoose";

const userschema = new mongoose.Schema({
  name: { type: String },
  leavereason: { type: Number },
  fromdate:{type: Date},
  todate:{type: Date},

});

const Mongodb = mongoose.model("user", userschema);
export default Mongodb;
