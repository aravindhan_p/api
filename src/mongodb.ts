import mongoose from "mongoose";

const url = `mongodb+srv://aravindhan:aravindhan@cluster0.2uqa1.mongodb.net/<dbname>?retryWrites=true&w=majority`;
const connectionParams = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
};

const dbconnection = () => {
  mongoose
    .connect(url, connectionParams)
    .then(() => {
      console.log("Connected to database ");
    })
    .catch((err) => {
      console.error(`Error connecting to the database. \n${err}`);
    });
};

export default dbconnection;
