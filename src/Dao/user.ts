import Mongodb from "../Modal/userModal";

export class userDao {
  public async CreateUser(body: any) {
    console.log("Body", body);
    const new_user = new Mongodb(body);
    return await new_user.save();
  }

  public async getuser() {
    console.log("Dao get");
    return await Mongodb.find();
  }
  public async Update(id: any, body: any) {
    const data: any = await Mongodb.findById(id);
    data.username = body.username;
    const userid: any = data._id;
    data.city = body.city;
    console.log("user", data);

    //
    return await Mongodb.updateOne({ userid }, { data });
  }
  public async delete(id: any) {
    const getdata: any = await Mongodb.findById(id);
    if (getdata) {
      const d =  getdata.remove();
      if(d) return "delete sucessfully....";
    }
    else return "no data found....";
  }
}
