
import express from "express";
import bodyParser from "body-parser";
import dbconnection from"../src/mongodb";
import { Routes } from "./Routes/Routes";

const PORT = process.env.PORT || 5000;

class Server {
  public app = express();
  public routePrv: Routes = new Routes();

  constructor() {
    this.MiddlewareSetup();
    dbconnection();
    this.routePrv.initialRoutes(this.app);
  }

  private MiddlewareSetup() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }
}

new Server().app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
