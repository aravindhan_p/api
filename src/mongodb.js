"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var url = "mongodb+srv://aravindhan:aravindhan@cluster0.2uqa1.mongodb.net/<dbname>?retryWrites=true&w=majority";
var connectionParams = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
};
var dbconnection = function () {
    mongoose_1.default
        .connect(url, connectionParams)
        .then(function () {
        console.log("Connected to database ");
    })
        .catch(function (err) {
        console.error("Error connecting to the database. \n" + err);
    });
};
exports.default = dbconnection;
