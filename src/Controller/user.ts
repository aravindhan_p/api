import { Request, Response } from "express";
import { userService } from "../Service/user";

const userservice = new userService();

export class userController {
  public async CreateUser(req: Request, res: Response) {
    const { body } = req;
    console.log("body", body);
    const user = await userservice.CreateUser(body);
    res.json(user);
  }

  public async getuser(req: Request, res: Response) {
    //const id = req.params.id;
    console.log("controller");
    const user = await userservice.getuser();
    res.json(user);
  };
  public async Update(req: Request, res: Response) {
    const id = req.params.id;
    const { body } = req;
    console.log(body);
    const user = await userservice.Update(id, body);
    res.json(user);
}
public async delete(req: Request, res: Response) {
  const id = req.params.id;
  console.log(id);
  const user = await userservice.delete(id);
  res.json(user);
}
}