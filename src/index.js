"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var mongodb_1 = __importDefault(require("../src/mongodb"));
var Routes_1 = require("./Routes/Routes");
var PORT = process.env.PORT || 5000;
var Server = /** @class */ (function () {
    function Server() {
        this.app = express_1.default();
        this.routePrv = new Routes_1.Routes();
        this.MiddlewareSetup();
        mongodb_1.default();
        this.routePrv.initialRoutes(this.app);
    }
    Server.prototype.MiddlewareSetup = function () {
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
    };
    return Server;
}());
new Server().app.listen(PORT, function () {
    console.log("Listening on port " + PORT);
});
